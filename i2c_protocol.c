#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <linux/i2c-dev.h>

//#define _DEBUG_UART
#define MAX_BUF_LEN 2048

typedef enum _cmd_id {
	CMD_ID_VERSION = 0x00,
	CMD_ID_GET_SENSOR_ID = 0x01,
	CMD_ID_GET_STREAM_INFO = 0x02,
	CMD_ID_GET_CTRL_INFO = 0x03,
	CMD_ID_INIT_CAM = 0x04,
	CMD_ID_GET_STATUS = 0x05,
	CMD_ID_DE_INIT_CAM = 0x06,
	CMD_ID_STREAM_ON = 0x07,
	CMD_ID_STREAM_OFF = 0x08,
	CMD_ID_STREAM_CONFIG = 0x09,
	
	/* Reserved 0x0A to 0x0F */
	
	CMD_ID_GET_CTRL = 0x10,
	CMD_ID_SET_CTRL = 0x11,
	CMD_ID_ISP_READ = 0x12,
	CMD_ID_ISP_WRITE = 0x13,
	CMD_ID_FW_UPDT = 0x14,
	CMD_ID_ISP_PDOWN = 0x15,
	CMD_ID_ISP_PUP = 0x16,
	
	/* Reserved - 0x17 to 0xFE (except 0x43) */
	
	CMD_ID_UNKNOWN = 0xFF,
	
} HOST_CMD_ID;

unsigned char calculatecrc(char *data,  unsigned int len)
{
	unsigned int i = 0;
	unsigned char crc = 0x00;

	for( i = 0; i < len; i++) {
		crc ^= data[i];
	}

	return crc;
}

int set_interface_attribs(int fd, int slaveaddr)
{
	int ret;
	ret = ioctl(fd, I2C_SLAVE_FORCE, slaveaddr);
	if (ret < 0) {
		printf("Set Slave Address Error - %s \n", strerror(errno));
		return ret;
	}
	return 0;
}

int open_port(int *fd, char *port)
{
	struct termios newtio;
			
	*fd = open(port, O_RDWR);
	if(*fd < 0) {
		return *fd;
	}

	return 0;
}
int printstring(unsigned char *string, unsigned int string_length)
{
	unsigned int pos;
	printf("\n--------------------------------------------STARTED-------------------------------------------------\n");

	for(pos=0;pos<string_length;pos++)
	{
		printf("0x%02x-%c ",string[pos],(string[pos] >= 32)?((string[pos] <= 126)?string[pos]:' '):' ');
		if((((pos+1)%20)==0))
			printf("\n");
			
	}
		
	printf("\n--------------------------------------------ENDED---------------------------------------------------\n");
	return 0;
}


void usage(char *appname)
{
	printf ("Usage : %s \n\n", appname);
	printf ("-i2cport /dev/i2c-0\n");
	printf ("-slaveaddr <0xaddr>\n");
	printf ("\n\n");
	return;
}

unsigned char i2cport[MAX_BUF_LEN] = {0};
unsigned char hexfile[4*MAX_BUF_LEN] = {0};
unsigned int slaveaddr;
unsigned char v4l2cmd[MAX_BUF_LEN] = {0};
unsigned int cmd = 0;
int parse_args(int argc, char **argv)
{
	int arg, i;
	if (argc == 1) {
		usage(argv[0]);
		return -1;
	}
	for (arg = 1; arg < argc; arg++) {
		if (strcmp(argv[arg], "-i2cport") == 0) {
			arg++;
			strcpy(i2cport, argv[arg]);
		}else if (strcmp(argv[arg], "-slaveaddr") == 0) {
			arg++;
			sscanf(argv[arg], "0x%x", &slaveaddr);
		}
	}
	return 0;
}

int main (int argc, char **argv)
{
	int fd, ret;
	unsigned char wbuf[MAX_BUF_LEN];
	unsigned char rbuf[MAX_BUF_LEN];
	int choice = 0;
	int wlen, rlen, retry = 10;
	unsigned short int index = 0;

	if (parse_args(argc, argv) < 0) {
		printf("Error : Application Input argument required/mismatch\n");
		exit(0);
	}

	ret = open_port(&fd, i2cport);
	if (ret < 0) {
		printf("Failed to open device port\n");
	}
	/* Set slave address */
	set_interface_attribs(fd, slaveaddr);

	while(1) {
		printf(" 1. Get FW Version  \n"
				" 2. Get Sensor ID   \n"
				" 3. Get Stream config \n"
				" 4. Get Control config \n"
				" 5. Init Cam \n"
				" 6. Get CMD Status \n"
				" 7. De-Init Cam \n"
				" 8. Stream on \n"
				" 9. Stream Off \n"
				" 10. Stream Config \n"
				" 11. Get Control value \n"
				" 12. Set Control Value \n"
				" 13. ISP Read \n"
				" 14. ISP Write \n"
				" 15. Jump to Bootloader \n"
				" 16. ISP Power Down \n"
				" 17. ISP Wake up \n"
				" 99. Exit \n"
				" Enter Choice : ");
		scanf("%d", &choice);
		//choice = 3;

		switch(choice) {
			case 1:

				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_VERSION;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_VERSION;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif

				printf("Trying to read from port \n");
				ret = read(fd, v4l2cmd,6);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);
#ifdef _DEBUG_UART
				sleep(1);
#endif

				cmd = v4l2cmd[2] << 8 | v4l2cmd[3];


				printf("Trying to read from port - %hu \n", cmd + 4);
				ret = read(fd, v4l2cmd, cmd + 4);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);			
				break;

			case 2:


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_GET_SENSOR_ID;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_GET_SENSOR_ID;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif

				printf("Trying to read from port \n");
				ret = read(fd, v4l2cmd,6);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);
#ifdef _DEBUG_UART
				sleep(1);
#endif

				usleep(2000);
				cmd = v4l2cmd[2] << 8 | v4l2cmd[3];


				printf("Trying to read from port - %hu \n", cmd + 4);
				ret = read(fd, v4l2cmd, cmd + 4);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);			

				break;


			case 3:
				index = 0;
				while(1) {
					v4l2cmd[0] = 0x43;		
					v4l2cmd[1] = CMD_ID_GET_STREAM_INFO;		
					// MSB first
					v4l2cmd[2] = 0x00;		
					v4l2cmd[3] = 0x02;

					v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
					printstring(v4l2cmd, 5);

					ret = write (fd, v4l2cmd, 5);
					if(ret < 0) {
						printf("Write Failed - %s \n", strerror(errno));
						close(fd);
						exit(0);
					} 
					printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
					sleep(1);
#endif


					v4l2cmd[0] = 0x43;		
					v4l2cmd[1] = CMD_ID_GET_STREAM_INFO;		
					v4l2cmd[2] = index >> 8;		
					v4l2cmd[3] = index & 0xFF;

					v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);					
					printstring(v4l2cmd, 5);

					ret = write (fd, v4l2cmd, 5);
					if(ret < 0) {
						printf("Write Failed - %s \n", strerror(errno));
						close(fd);
						exit(0);
					}

					printf(" Write 2 Success \n");

#ifdef _DEBUG_UART
					sleep(1);
#endif

					printf("Trying to read from port \n");
					ret = read(fd, v4l2cmd,6);
					if (ret < 0) {
						printf("Read Failed - %s \n", strerror(errno));
						close(fd);
						exit(0);
					} 
					printstring(v4l2cmd, ret);
#ifdef _DEBUG_UART
					sleep(1);
#endif

					cmd = v4l2cmd[2] << 8 | v4l2cmd[3];

					if(cmd == 0)
						break;

					printf("Trying to read from port - %hu \n", cmd + 4);
					ret = read(fd, v4l2cmd, cmd + 4);
					if (ret < 0) {
						printf("Read Failed - %s \n", strerror(errno));
						close(fd);
						exit(0);
					} 
					printstring(v4l2cmd, ret);						
					index += 1;

#ifdef _DEBUG_UART
					sleep(1);
#endif

				}
				break;

			case 4:
				index = 0;
				while(1) {
					v4l2cmd[0] = 0x43;		
					v4l2cmd[1] = CMD_ID_GET_CTRL_INFO;		
					// MSB first
					v4l2cmd[2] = 0x00;		
					v4l2cmd[3] = 0x02;

					v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
					printstring(v4l2cmd, 5);

					ret = write (fd, v4l2cmd, 5);
					if(ret < 0) {
						printf("Write Failed - %s \n", strerror(errno));
						close(fd);
						exit(0);
					} 
					printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
					sleep(1);
#endif


					v4l2cmd[0] = 0x43;		
					v4l2cmd[1] = CMD_ID_GET_CTRL_INFO;		
					v4l2cmd[2] = index >> 8;		
					v4l2cmd[3] = index & 0xFF;

					v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);					
					printstring(v4l2cmd, 5);

					ret = write (fd, v4l2cmd, 5);
					if(ret < 0) {
						printf("Write Failed - %s \n", strerror(errno));
						close(fd);
						exit(0);
					}

					printf(" Write 2 Success \n");

#ifdef _DEBUG_UART
					sleep(1);
#endif

					printf("Trying to read from port \n");
					ret = read(fd, v4l2cmd,6);
					if (ret < 0) {
						printf("Read Failed - %s \n", strerror(errno));
						close(fd);
						exit(0);
					} 
					printstring(v4l2cmd, ret);
#ifdef _DEBUG_UART
					sleep(1);
#endif

					cmd = v4l2cmd[2] << 8 | v4l2cmd[3];

					if(cmd == 0)
						break;

					printf("Trying to read from port - %hu \n", cmd + 4);
					ret = read(fd, v4l2cmd, cmd + 4);
					if (ret < 0) {
						printf("Read Failed - %s \n", strerror(errno));
						close(fd);
						exit(0);
					} 
					printstring(v4l2cmd, ret);						
					index += 1;

#ifdef _DEBUG_UART
					sleep(1);
#endif

				}
				break;

			case 5:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_INIT_CAM;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_INIT_CAM;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");
				break;

			case 6:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_GET_STATUS;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x01;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");
#ifdef _DEBUG_UART
				sleep(1);
#endif				
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_GET_STATUS;		
				v4l2cmd[2] = CMD_ID_UNKNOWN;		
				printstring(v4l2cmd, 3);

				ret = write (fd, v4l2cmd, 3);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");				
#ifdef _DEBUG_UART
				sleep(1);
#endif				

				printf("Trying to read from port \n");
				ret = read(fd, v4l2cmd,7);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);
#ifdef _DEBUG_UART
				sleep(1);
#endif
				break;

			case 7:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_DE_INIT_CAM;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_DE_INIT_CAM;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");				
				break;

			case 8:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_STREAM_ON;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_STREAM_ON;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");				
				break;

			case 9:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_STREAM_OFF;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_STREAM_OFF;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");				
				break;

			case 10:
				index = 0;
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_STREAM_CONFIG;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x0E;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif

				// 0x43-C 0x02-  0x56-V 0x59-Y 0x55-U 0x59-Y 0x02-  0x80-  0x01-  0xe0-  0x00-  0x00-  0x1e-  0x00-  0x01-  0x7f-  0x00-
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_STREAM_CONFIG;		
				v4l2cmd[2] = index >> 8;
				v4l2cmd[3] = index & 0xFF;
				// UYVY
				v4l2cmd[4] = 0x56 ;
				v4l2cmd[5] = 0x59 ;
				v4l2cmd[6] = 0x55 ;
				v4l2cmd[7] = 0x59 ;

				// Width - 640
				v4l2cmd[8] = 0x02 ;
				v4l2cmd[9] = 0x80 ;

				// Height - 480
				v4l2cmd[10] = 0x01 ;
				v4l2cmd[11] = 0xE0 ;

				// Frame Rate Num - 30
				v4l2cmd[12] = 0x00 ;
				v4l2cmd[13] = 0x1E ;

				// Frame Rate Denom - 1
				v4l2cmd[14] = 0x00 ;
				v4l2cmd[15] = 0x01 ;
				v4l2cmd[16] = calculatecrc(&v4l2cmd[2], 14) ;
				printstring(v4l2cmd, 17);

				ret = write (fd, v4l2cmd, 17);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");				
				break;

			case 11:
				index = 0;
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_GET_CTRL;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x02;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_GET_CTRL;		
				v4l2cmd[2] = index >> 8;		
				v4l2cmd[3] = index & 0xFF; 
				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		

				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif

				printf("Trying to read from port \n");
				ret = read(fd, v4l2cmd,6);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);
#ifdef _DEBUG_UART
				sleep(1);
#endif

				cmd = v4l2cmd[2] << 8 | v4l2cmd[3];

				printf("Trying to read from port - %hu \n", cmd + 4);
				ret = read(fd, v4l2cmd, cmd + 4);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);						

#ifdef _DEBUG_UART
				sleep(1);
#endif				
				break;

			case 12:
				index = 0;

				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_SET_CTRL;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x0B;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif

				/* 0x43-C 0x03-  0x00-  0x98-  0x09-  0x00-  0x00-  0xff-  0xff-  0xff-  0xf1-  0x00-  0x00-  0x00-  0x0f-  0x00-  0x00-  0x00-  0x00-  0x00-  
0x00-  0x00-  0x01-  0x91-  0x00- */
				
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_SET_CTRL;		

				v4l2cmd[2] = index >> 8 ;
				v4l2cmd[3] = index & 0xFF ;

				// Ctrl ID - 0x00 0x98 0x09 0x00
				v4l2cmd[4] = 0x00 ;
				v4l2cmd[5] = 0x98 ;
				v4l2cmd[6] = 0x09 ;
				v4l2cmd[7] = 0x00 ;

				// Ctrl Type - 0x01 (standard)
				v4l2cmd[8] = 0x01 ;
				
				// Value - 0x00000000 
				v4l2cmd[9] = 0x00 ;
				v4l2cmd[10] = 0x00 ;
				v4l2cmd[11] = 0x00 ;
				v4l2cmd[12] = 0x00 ;

				v4l2cmd[13] = calculatecrc(&v4l2cmd[2], 11) ;
				printstring(v4l2cmd, 14);

				ret = write (fd, v4l2cmd, 14);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");								
				break;

			case 13:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_ISP_READ;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x03;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_ISP_READ;		

				/* Register 0x2000 - Width */
				v4l2cmd[2] = 0x20;		
				v4l2cmd[3] = 0x00;		

				/* Register Length - 2 Bytes */
				v4l2cmd[4] = 0x02;		

				v4l2cmd[5] = calculatecrc(&v4l2cmd[2], 3);		
				printstring(v4l2cmd, 6);

				ret = write (fd, v4l2cmd, 6);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif

				usleep(2000);
				printf("Trying to read from port \n");
				ret = read(fd, v4l2cmd,6);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);
#ifdef _DEBUG_UART
				sleep(1);
#endif

				cmd = v4l2cmd[2] << 8 | v4l2cmd[3];


				printf("Trying to read from port - %hu \n", cmd + 4);
				ret = read(fd, v4l2cmd, cmd + 4);
				if (ret < 0) {
					printf("Read Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printstring(v4l2cmd, ret);							
				break;

			case 14:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_ISP_WRITE;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x05;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif

				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_ISP_WRITE;		

				/* Register - 0x2000 (width) */
				v4l2cmd[2] = 0x20 ;
				v4l2cmd[3] = 0x00 ;

				/* Register length - 2 Bytes */
				v4l2cmd[4] = 0x02 ;

				/* Data - 2 bytes */
				v4l2cmd[5] = 1920 >> 8;
				v4l2cmd[6] = 1920 & 0xFF;

				v4l2cmd[7] = calculatecrc(&v4l2cmd[2], 5) ;
				printstring(v4l2cmd, 8);

				ret = write (fd, v4l2cmd, 8);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");	
				break;

			case 15:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_FW_UPDT;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_FW_UPDT;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");								
				sleep(1);
				close(fd);
				exit(0);				
				break;

			case 16:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_ISP_PDOWN;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_ISP_PDOWN;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");								
				break;

			case 17:
				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_ISP_PUP;		
				// MSB first
				v4l2cmd[2] = 0x00;		
				v4l2cmd[3] = 0x00;

				v4l2cmd[4] = calculatecrc(&v4l2cmd[2], 2);		
				printstring(v4l2cmd, 5);

				ret = write (fd, v4l2cmd, 5);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				} 
				printf(" Write 1 Success \n");

#ifdef _DEBUG_UART
				sleep(1);
#endif


				v4l2cmd[0] = 0x43;		
				v4l2cmd[1] = CMD_ID_ISP_PUP;		
				printstring(v4l2cmd, 2);

				ret = write (fd, v4l2cmd, 2);
				if(ret < 0) {
					printf("Write Failed - %s \n", strerror(errno));
					close(fd);
					exit(0);
				}

				printf(" Write 2 Success \n");								
				break;


			case 99:
				goto exit;
		}	
	}

exit:
	close(fd);
	exit(0);

}

