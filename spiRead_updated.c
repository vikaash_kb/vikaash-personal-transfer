#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

int main() {
	
	
	int block_size = 10;
	uint32_t mode = 0;
	uint8_t bits = 8;
	uint32_t speed = 500000;
	uint8_t rx[block_size];
	int ret;
	int fd;
    	int i;
    	char c;
        memset(&rx,0,block_size);
	printf("before read\n");
	fd = open("/dev/spidev0.0", O_RDONLY);
	if(fd==-1)
	{
		printf("spi dev open failed");
	}
	// Example mode. Just use the modes you need if any.
	// mode = SPI_CPHA | SPI_CPOL | SPI_LSB_FIRST | SPI_LSB_FIRST | SPI_NO_CS;
	ret = ioctl(fd, SPI_IOC_WR_MODE32, &mode);
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	

	while(1){
	ret = read(fd, rx, block_size);
	
    
	for(i=0;i<block_size;i++){
            printf("i: %d  0x%2x \n",i,rx[i]);
    }
	printf("------- End of FRAME ---------\n");
	}

        close(fd);
	return 0;

}
